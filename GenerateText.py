#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 16:15:33 2020

@author: aayush
"""


from pickle import load
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences


model = load_model("Weights/weights-improvement-57-0.6618.hdf5")
tokenizer = load(open("tokenizer.pkl", "rb"))
def load_doc(filename):
	# open the file as read only
	file = open(filename, 'r')
	# read all text
	text = file.read()
	# close the file
	file.close()
	return text
 
# load
in_filename = 'seed_preprocessed.txt'
doc = load_doc(in_filename)
lines = doc.split('\n')
seed_text = lines[0]


seq_length = 99





print(seed_text + '\n')
n_doc = 1000
out_word = ""
for j in range(n_doc):
    encoded = tokenizer.texts_to_sequences([seed_text])[0]
    encoded = pad_sequences([encoded], maxlen=seq_length, truncating='pre')
    out = model.predict_classes(encoded , verbose=0)
    
    for w,i in tokenizer.word_index.items():
        if i == out:
            out_word+=w+" "
            break
        
            
    seed_text += " " + w
    
    
print(out_word)

open("Output.txt" , "w").write(out_word)
    
